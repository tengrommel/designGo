# 接口

## duck typing

- 描述事物的外部行为而非内部结构
- 严格说go属于结构化类型系统，类似duck typing

接口的定义

-接口由使用者定义

接口的实现

- 接口的实现是隐式的
- 只要实现接口里的方法

查看接口变量

- 表示任何类型: interface{}
- Type Assertion
- Type Switch