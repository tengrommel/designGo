# http

- 使用http客户端发送请求
- 使用http.Client控制请求头部等
- 使用httputil简化工作

# http服务器的性能分析

- import _ "net/http/pprof"
- 访问/debug/pprof/
- 使用 go tool pprof分析性能

# 其他的标准库
- bufio
- log
- encoding/json
- regexp
- time
- strings/math/rand

## 查看文档

- godoc -http :8888
