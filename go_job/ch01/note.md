#GO 语言的特点

- 没有 "对象"，没有继承多态，没有泛型，没有try/catch
- 有接口，函数式编程，CSP并发模型(goroutine + channel)

# Go语言基本语法

## 內建变量类型

- bool, string
- (u)int,(u)int8,(u)int16,(u)int32,(u)int64,uintptr
- byte,rune
- float32,float64,complex64,complex128

## 强制类型转换
- 类型转换是强制的
- var a, b int = 3, 4

## 常量的定义

## 枚举类型

## switch 
> switch会自动break,除非使用fallthrough

## for

- for 的条件里不需要括号
- for的条件里可以省略初始条件，结束条件，递增表达式

## 函数

- 函数返回多个值时可以起名字
- 仅用于非常简单的函数
- 对于调用者而言没有区别

## 指针

- 指针不能运算

## 参数传递
- Go只有值传递一种方式

