package _4_longest_common_prefix

func longestCommonPrefix(strs []string) string {
	short := shortest(strs)
	for i, r := range short {
		for j := 0; i < len(strs); j++ {
			if strs[j][i] != byte(r) {
				return strs[j][:i]
			}
		}
	}
	return short
}

func shortest(strs []string) string {
	if len(strs) == 0 {
		return ""
	}

	res := strs[0]
	for _, s := range strs {
		if len(res) > len(s) {
			res = s
		}
	}
	return res
}
