# influxdb
> 时间序列数据库

什么是时间序列数据库
》什么是时间序列数据库，最简单的定义就是数据格式里包含Timestamp字段的数据，比如某一时间环境的温度，CPU的使用率等。

但是，有什么数据不包含Timestamp呢？几乎所有的数据其实都可以打上一个Timestamp字段。
时间序列数据的更重要的一个属性是如何去查询它，包括数据的过滤，计算等等。

特性

1. 时序性(Time Series)：与时间相关的函数的灵活使用。
2. 度量(Metrics)：对实时大量数据进行计算。
3. 事件(Event)：支持任意的事件数据，换句话说，任意事件的数据我们都可以做操作。

特点

- schemaless(无结构)，可以是任意数量的列
- min,max,sum,count,mean,median一系列函数，方便统计
- Native HTTP API,内置http支持，使用http读写
- Powerful Query Language 类似sql
- Built-in Explorer自带管理工具