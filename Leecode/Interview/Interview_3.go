package main

import "fmt"

type People struct {}

func (p *People)ShowA()  {
	fmt.Println("showA")
}

func (p *People)ShowB()  {
	fmt.Println("showB")
}

type Teacher struct {
	People
}

func (t *Teacher)ShowB()  {
	fmt.Println("teacher showB")
}

/*
这是Golang的组合模式，可以实现OOP的继承。
被组合的类型People所包含的方法虽然升级成了外部类型Teacher这个组合类型的方法（一定要是匿名字段），但它们
的方法(ShowA())调用时接受者并没有发生变化。
 */

func main() {
	t := Teacher{}
	t.ShowA()
	t.ShowB()
}
