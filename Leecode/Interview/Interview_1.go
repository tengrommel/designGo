package main

import "fmt"

type student struct {
	Name string
	Age int
}

// 错误的写法
func pase_student()  {
	m := make(map[string]*student)
	stus := []student{
		{Name: "zhou", Age: 24},
		{Name: "li", Age: 23},
		{Name: "wang", Age: 22},
	}
	for _, stu := range stus{
		m[stu.Name] = &stu
	}
	fmt.Println(m)
}

/*
这样的写法初学者经常会遇到的，很危险！
与java的foreach一样，都是使用副本的方式。
for range创建了每个元素的副本，而不是直接返回每个元素的引用，
如果使用该值变量的地址作为指向每个元素的指针，就会导致报错。
*/

func pass_student() {
	m := make(map[string]*student)
	stus := []student{
		{Name:"zhou", Age:24},
		{Name:"li", Age:23},
		{Name:"wang", Age:22},
	}

	for i:=0;i<len(stus);i++ {
		m[stus[i].Name] = &stus[i]
	}
	for k, v := range m{
		fmt.Print(k,"=>",v.Name)
		fmt.Println("&",v)
	}
}

func main() {
	pase_student()
	pass_student()
}
