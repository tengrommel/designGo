package main

import "fmt"

func GetValue_(m map[int]string, id int) (string, bool) {
	if _, exist := m[id]; exist {
		return "存在数据", true
	}
	//return nil, false
	/*nil 可以用作interface、function、pointer、map、slice和channel的"空值"*/
	return "", false
}

func main() {
	intmap := map[int]string{
		1:"a",
		2:"bb",
		3:"ccc",
	}

	v, err := GetValue_(intmap, 3)
	fmt.Println(v, err)
}
