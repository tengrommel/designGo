package main

import (
	"gitlab.com/tengrommel/designGo/crawler/engine"
	"gitlab.com/tengrommel/designGo/crawler/zhenai/parser"
)

func main() {
	engine.Run(engine.Request{
		Url:        "http://www.zhenai.com/zhenghun",
		ParserFunc: parser.ParseCityList,
	})
}
