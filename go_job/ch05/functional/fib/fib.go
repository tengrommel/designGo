package main

import (
	"gitlab.com/tengrommel/designGo/go_job/ch05/functional/fib/fb"
)

// 1,2,3,5,8,13,...
// a, b
//

func main() {
	f := fb.Fibonacci()
	fb.PrintFileContents(f)
}
