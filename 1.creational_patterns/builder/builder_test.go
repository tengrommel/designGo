package builder

import "testing"

func TestBuilderPattern(t *testing.T)  {
	// 构造器
	manufacturingComplex := ManufacturingDirector{}

	// 初始构造器 并加入构造结构体
	carBuilder := &CarBuilder{}
	manufacturingComplex.SetBuilder(carBuilder)
	manufacturingComplex.Construct()

	car := carBuilder.GetVehicle()

	if car.Wheels != 4 {
		t.Errorf("Wheels on a car must be 4 and they were %d\n", car.Wheels)
	}
	if car.Structure != "Car" {
		t.Errorf("Structure on a car must be 'Car' and was %s\n", car.Structure)
	}
	if car.Seats != 5 {
		t.Errorf("Seats on a car must be 5 and they were %d\n", car.Seats)
	}

	// bike
	bikeBuilder := &BikeBuilder{}
	manufacturingComplex.SetBuilder(bikeBuilder)
	manufacturingComplex.Construct()

	bike := bikeBuilder.GetVehicle()

	if bike.Wheels != 2{
		t.Errorf("Wheels on a motorbike must be 2 and they were %d\n", bike.Wheels)
	}
	if bike.Structure != "Motorbike" {
		t.Errorf("Structure on a Motorbike must be 'Motorbike' and was %s\n", bike.Structure)
	}
	if bike.Seats != 2{
		t.Errorf("Seets on a motorbike must be 2 and they were %d\n", bike.Seats)
	}

	// bus 接口必须对应地址 而不是实体
	busBuilder := &BusBuilder{}
	manufacturingComplex.SetBuilder(busBuilder)
	manufacturingComplex.Construct()

	bus := busBuilder.GetVehicle()

	if bus.Wheels != 8{
		t.Errorf("Wheels on a bus must be 8 and they were %d\n", bus.Wheels)
	}
	if bus.Structure != "Bus" {
		t.Errorf("Structure on a Bus must be 'Bus' and it was %s\n", bus.Structure)
	}
	if bus.Seats != 30{
		t.Errorf("Seets on a motorbike must be 30 and they were %d\n", bus.Seats)
	}
}