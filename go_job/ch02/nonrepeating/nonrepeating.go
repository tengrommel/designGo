package nonrepeating

import "fmt"

/*
1. lastOccurred[x]不存在，或者<start->无需操作
2. lastOccurred[x]>=start->更新start
3. 更新lastOccurred[x],更新maxLength
*/

func lengthOfNonRepeatingSubStr(s string) int {
	//lastOccurred := make(map[rune]int)
	lastOccurred := make([]int, 0xffff)
	for i := range lastOccurred {
		lastOccurred[i] = -1
	}
	//lastOccurred['e'] = 1
	//lastOccurred['课']= 6
	start := 0
	maxLength := 0

	for i, ch := range []rune(s) {

		if lastI := lastOccurred[ch]; lastI != -1 && lastI >= start {
			start = lastI + 1
		}
		if i-start+1 > maxLength {
			maxLength = i - start + 1
		}
		lastOccurred[ch] = i
	}
	return maxLength
}

func main() {
	fmt.Println(lengthOfNonRepeatingSubStr("dfdafdf"))
	fmt.Println(lengthOfNonRepeatingSubStr("dfdafdf"))
	fmt.Println(lengthOfNonRepeatingSubStr("dfdafdf"))
	fmt.Println(lengthOfNonRepeatingSubStr("dfdafdf"))
}
