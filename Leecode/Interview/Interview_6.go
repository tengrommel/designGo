package main

import (
	"sync"
	"fmt"
)

type UserAges struct {
	ages map[string]int
	sync.Mutex
}

func (ua *UserAges)Add(name string, age int)  {
	ua.Lock()
	defer ua.Unlock()
	ua.ages[name] = age
}

/* map线程安全
 fatal error: concurrent map read and map write
*/
func (ua *UserAges)Get(name string) int {
	ua.Lock()
	defer ua.Unlock()
	if age, ok := ua.ages[name]; ok{
		return age
	}
	return -1
}

func main() {
	user := UserAges{}
	user.ages = make(map[string]int)
	fmt.Println(user)
	user.Add("teng", 12)
	fmt.Println(user)
	fmt.Println(user.Get("teng"))
}
