package main

import "fmt"

func main() {
	// 要创建一个空map，需要使用內建的`make`:
	// `make(map[key-type]val-type)`
	m := make(map[string]int)

	// 使用典型的`make[key]=val`语法来设置键值对
	m["k1"]=7
	m["k2"]=13

	// 使用例如`Println`来打印一个map将会输出所有的键值对。
	fmt.Println("map:", m)

	// 使用`name[key]`来获取一个键的值
	v1 := m["k1"]
	fmt.Println("v1: ", v1)

	// 当对一个map调用內建的`len`时，返回的是键值对的个数
	fmt.Println("LEN:", len(m))

	// 內建的`delete`可以从一个map中删除一个键值对
	delete(m, "k2")
	fmt.Println("map:", m)

	/*
	当从一个map中取值时，可选的第二返回值指示这个键是在这个map中。
	这可以用来消除键不存在和键有零值，像`0`或者`""`而产生的歧义。
	 */
	 _, prs := m["k2"]
	 fmt.Println("prs:", prs)

	 // 你也可以通过这个语法在同一行申明和初始化一个新的map。
	 n := map[string]int{"foo":1, "bar":2}
	 fmt.Println("map:", n)
}
