package main

import "fmt"

func printArray(arr []int)  {
	arr[0] = 100
	for _, v:=range arr{
		fmt.Println(v)
	}
}

func main() {
	var arr1 [5]int
	arr2 := [3]int{1,3,5}
	arr3 := [...]int{2,4,6,8,10}
	var grid [4][5]int

	fmt.Println(arr1, arr2, arr3)
	fmt.Println(grid)

	fmt.Println("printArray(array1)")
	printArray(arr3[:])
	//printArray(arr2)
	fmt.Println("printArray(array3)")
	printArray(arr3[:])

	fmt.Println("print array1 and array3")
	fmt.Println(arr1)
	fmt.Println(arr3)

}
