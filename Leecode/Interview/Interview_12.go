package main

import "fmt"

func main() {
	//list := new([]int) // 编译错误
	list := make([]int,0)
	list = append(list, 1)
	fmt.Println(list)
}
