# Creational Patterns

As the name implies,it groups common practices for creating objects,so object creation is more encapsulated
from the users that need those objects.

Mainly,creational patterns try to give ready-to-use objects to users instead of asking for their creation,
which,in some cases,could be complex,or which would couple your code with the concrete implementations of 
the functionality that should be defined in an interface.

