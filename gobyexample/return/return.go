package main

import "fmt"

// `(int, int)`在这个函数中标志着这个函数返回2个`int`
func vals() (int, int) {
	return 3, 7
}

func main() {
	a, b := vals()
	fmt.Println(a)
	fmt.Println(b)

	_, c := vals()
	fmt.Println(c)
}
