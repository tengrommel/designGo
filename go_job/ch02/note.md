# array
> 数组是值类型

# Slice(切片)
> 底层数组的地址

Slice本身没有数据，是对底层array的一个view

- slice 可以向后扩展，不可以向前扩展
- s[i]不可以超越len(s)，向后扩展不可以超越底层数组cap(s)

# 向slice添加元素

- 添加元素时如果超越cap，系统会重新分配更大的底层数组

- 由于值传递的关系，必须接收append的返回值

# Map
>map[K]V, map[K1]map[K2]V

## Map的操作
- 创建： make(map[string]int)
- 获取元素： m[key]
- key不存在时，获得Value类型的初始值
- 用value,ok:=m[key]来判断是否存在key
- 用delete删除一个key

## Map的遍历
- 使用range遍历key，或者遍历key,value对
- 不保证遍历顺序，如需顺序，需手动对key排序
- 使用len来获得元素的个数

*map的key*
- map使用哈希表，必须可以比较相等
- 除了slice,map,function的內建类型都可以作为key
- Struct类型不包含上述字段，也可以作为key

# rune相当于go的char
- 使用range遍历pos,rune对
- 使用utf8.RuneCountInString获得字符数量
- 使用len获得字节长度
- 使用[]byte获得字节