# Behavior design
> Correct Behavior design is the last step after knowing how to deal with object creation and structures.

## Strategy design pattern
> The Strategy pattern is probably the easiest to understand of the Behavioral patterns.

1. The Strategy pattern uses different algorithms to achieve some specific functionality.
2. These algorithms are hidden behind an interface and,of course,they must be interchangeable.
