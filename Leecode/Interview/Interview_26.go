package main

func test_(x int) (func(), func())  {
	return func() {
		println(x)
		x+=10
	}, func() {
		println(x)
	}
}

func main() {
	a, b := test_(100)
	a()
	b()
}
