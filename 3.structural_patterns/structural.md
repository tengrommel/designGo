# structural patterns

The Go language, by nature, encourages use of composition almost exclusively by its lack of inheritance.

Because of this, we have been using the Composite design pattern extensively until now, so let's start by
defining the Composite design pattern.

## Composite design pattern
>The Composite design pattern favors composition(commonly defined as a has a relationship) over inheritance
(an is a relationship).

We will learn how to create object structures by using a has a approach.

## Adapter design pattern

The Adapter pattern is very useful when,for example,an interface gets outdated and it's not possible to replace it easily of fast.

Adapter also helps us to maintain the open/closed principle in our apps, making them more predictable too. 


## Bridge design pattern

It decouples an abstraction from its implementation so that the two can vary independently.

This cryptic explanation just means that you could even decouple the most basic form of functionality:
decouple an object from what it does.

## Proxy design pattern

- The Proxy pattern usually wraps an object to hide some of its characteristics.
- These characteristics could be the fact that it is a remote object(remote proxy),
a very heavy object such as a very big image or the dump of a terabyte database(virtual proxy),
or a restricted access object(protection proxy).

## Decorator design pattern
> The Decorator design pattern allows you to decorate an already existing type with more functional features without actually touching it.

The Decorator type implements the same interface of the type it decorates,and stores an instance of that type in its members.

## Facade design pattern
> A facade,in architectural terms,is the front wall that hides the rooms and corridors of a building.<br>
It protects its inhabitants from cold and rain,and provides them privacy.

You use the Facade design pattern in the following scenarios:

- When you want to decrease the complexity of some parts of our code.<br>
You hide that complexity behind the facade by providing a more easy-to-use method.

- When you want to group actions that are cross-related in a single place.

- When you want to build a library so that others can use your products without worrying about how it all works.


## Flyweight design pattern
>It's very commonly used in computer graphics and the video game industry,but not so mush in enterprise applications.

- Flyweight is a pattern which allows sharing the state of a heavy object between many instance of some type .

- Imagine that you have to create and store too many objects of some heavy type that are fundamental equal.

