# 资源管理与出错处理

CATCH ALL THE ERRORS

# defer 调用

- 确保调用在函数结束时发生

- 参数在defer语句时计算

- defer列表为后进先出

# 如何实现统一的错误处理逻辑

# panic

- 停止当前函数执行

- 一直向上返回，执行每一层的defer

- 如果没有遇见recover，程序退出

# recover

- 仅在defer调用中使用
- 获取panic的值
- 如果无法处理，可重新panic