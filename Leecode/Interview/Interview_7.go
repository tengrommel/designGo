package main

import (
	"sync"
	"fmt"
)

type threadSafeSet struct {
	sync.RWMutex
	s []interface{}
}

func (thread_set *threadSafeSet) Iter() <-chan interface{} {
	ch := make(chan interface{}, len(thread_set.s))
	go func() {
		thread_set.RLock()

		for elem, value := range thread_set.s {
			ch <- elem
			println("Iter:", elem, value)
		}

		close(ch)
		thread_set.RUnlock()

	}()
	return ch
}

func main() {
	th := threadSafeSet{
		s:[]interface{}{"1", "2"},
	}
	v := <-th.Iter()
	fmt.Sprintf("%s%v", "ch", v)
}