package builder

type BuildProcess interface {
	SetWheels() BuildProcess
	SetSeats() BuildProcess
	SetStructure() BuildProcess
	GetVehicle() VehicleProduct
}

// Director
type ManufacturingDirector struct {
	builder BuildProcess
}

// 定义组装步骤并将接口函数和嵌入属性结合
func (f *ManufacturingDirector)Construct()  {
	f.builder.SetSeats().SetStructure().SetWheels()
}

// 设置构建者
func (f *ManufacturingDirector)SetBuilder(b BuildProcess)  {
	f.builder = b
}

// Product 定义常量
type VehicleProduct struct {
	Wheels				int
	Seats					int
	Structure 		string
}

//汽车结构体 实现创建接口
type CarBuilder struct {
	v VehicleProduct
}

func (c *CarBuilder)SetWheels() BuildProcess {
	c.v.Wheels = 4
	return c
}

func (c *CarBuilder)SetSeats() BuildProcess {
	c.v.Seats = 5
	return c
}

func (c *CarBuilder)SetStructure() BuildProcess {
	c.v.Structure = "Car"
	return c
}

func (c *CarBuilder)GetVehicle() VehicleProduct {
	return c.v
}

//摩托车结构体 实现创建接口
type BikeBuilder struct {
	v VehicleProduct
}

func (b *BikeBuilder) SetWheels() BuildProcess {
	b.v.Wheels = 2
	return b
}

func (b *BikeBuilder) SetSeats() BuildProcess {
	b.v.Seats = 2
	return b
}

func (b *BikeBuilder) SetStructure() BuildProcess {
	b.v.Structure = "Motorbike"
	return b
}

func (b *BikeBuilder) GetVehicle() VehicleProduct {
	return b.v
}

//公共汽车结构体 实现创建接口
type BusBuilder struct {
	v VehicleProduct
}

func (b *BusBuilder) SetWheels() BuildProcess {
	b.v.Wheels = 8
	return b
}

func (b *BusBuilder) SetSeats() BuildProcess {
	b.v.Seats = 30
	return b
}

func (b *BusBuilder) SetStructure() BuildProcess {
	b.v.Structure = "Bus"
	return b
}

func (b *BusBuilder) GetVehicle() VehicleProduct {
	return b.v
}
