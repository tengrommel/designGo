package main

import "fmt"

func main() {
	x := 1
	fmt.Println(x)
	{
		fmt.Println(x)
		x := 2
		fmt.Println(x)
	}
	fmt.Println(x)

	var m map[string]int
	m = make(map[string]int)
	m["one"]=1
	fmt.Println(len(m))

	var s []int
	s = append(s, 1)
	fmt.Println(s)

	var xx string

	if xx == ""{
		xx = "default"
	}
	fmt.Println(xx)
}
