package nonrepeating

import "testing"

func TestSubstr(t *testing.T) {
	tests := []struct {
		s   string
		ans int
	}{
		// Normal case
		{"adfdfa", 3},
		{"fyuff", 3},

		// Edge cases
		{"", 0},
		{"b", 1},

		// Chinese support
		{"这里是木块", 5},
		{"你们好", 3},
	}

	for _, tt := range tests {
		actual := lengthOfNonRepeatingSubStr(tt.s)
		if actual != tt.ans {
			t.Errorf("got %d for input %s; "+"expected %d", actual, tt.s, tt.ans)
		}
	}
}

func BenchmarkSubstr(b *testing.B) {
	s := "这里是木块"
	for i := 0; i < 13; i++ {
		s = s + s
	}

	ans := 5
	b.Logf("len(s) = %d", len(s))
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		actual := lengthOfNonRepeatingSubStr(s)
		if actual != ans {
			b.Errorf("got %d for input %s; "+"expected %d", actual, s, ans)
		}
	}
}
